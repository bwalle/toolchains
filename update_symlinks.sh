#!/bin/bash

TCDIR=/opt/toolchains

error() {
    echo >&2 "$@"
    exit 1
}

cd "$TCDIR"
if ! [ -d bin ] ; then
    error "Link dir $TCDIR/bin doesn't exist"
fi

echo -n "Deleting old links... "
rm -f bin/*
echo "done"

for toolchain in *-*-* ; do
    if ! [ -d "$toolchain/bin" ] ; then
        echo "$TCDIR/$toolchain doesn't contain a bin. Skipping."
        continue
    fi
    echo -n "Installing links for $toolchain... "

    for name in $(ls $toolchain/bin) ; do
        ln -s "../$toolchain/bin/$name" "bin/$name"
    done
    echo "done"
done


